---
Week: 37
Content:  COURSE WEEK CONTENT
Material: See links in weekly plan
Initials: PDA
---

# Week 37 Networking

## Goals of the week(s)
Pratical and learning goals for the period are as follows

### Practical goals

* See Assignment in Deliverables

### Learning goals

The students can at a basic level work with and explain:

* Network diagrams.  
* IP adresses on Linux.  
* VMware Workstation VMnets.

### Topics to be covered

* Walkthrough and Q&A for handed in homework.
* Network Diagrams. Network ID and and Host IPs.
* Two hosts - VM (Xububtu) installation.
* Set static IPs in GUI.
* How VMware Workstation works with VMnets = VMnet8 (NAT)/VMnet3 and Host Only.
* Linux Commands:
    * `$ ip a, tcpdump –D, tcpdump –i ens33 ` 
* Windows commands:
    * `\>route print`

## Deliverables

Assignment 2 VMnets, network diagram, static IP and traffic monitoring.

Please hand in on LMS.

## Schedule

Class B Tuesday/Class A Wednesday

## Hands-on time

* See deliverables.

## Comments

## Sources

TBD

## White Board

This weeks white board scribbles will often be posted here after the lectures  
![Week 37 White Board](../weekly_plans/Week37_VMware_two_host_on_a_VMnet_PDA_V01.jpg "Week 37 White Board scribbles")
Test  
