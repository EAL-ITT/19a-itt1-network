---
Week: 41
Content:  COURSE WEEK CONTENT
Material: See links in weekly plan
Initials: PDA
---

# Week 41-43 Networking

## Goals of the week(s)
Pratical and learning goals for the period are as follows

### Practical goals

* See Assignment in Deliverables

### Learning goals

The students can at a basic level work with and explain:

* The ethernet switch.  
* Linux as an ethernet switch.  
* Address Resolution Protocol - ARP.
* The Ethernet frame.
* The IP packet.

### Topics to be covered

* Walkthrough and Q&A for handed in homework.
* Network Diagrams. Network ID and and Host IPs.
* MAC addresses
* IP addresses.
* The Ethernet frame.
* The IP packet.
* Address Resolution Protocol - ARP.
* Using TCP dump to monitor traffic.
* Wireshark.

Linux commands:  

* $ arp See more commands in assignment in deliverables.  

Windows commands:  

* > arp

## Deliverables

OLA18_ITT1: (Obligatory Learning Activity)

Assignment 4 The Switch and the ARP and the tcpdump

Please hand in on both PeerGrade and WiseFlow. I.e. you have to give the all important Peer Feedback to each other  on PeerGrade and the WiseFlow hand in is only for the likewise all important registration of your hand in.

## Schedule

Class B Tuesday/Class A Wednesday

## Hands-on time

* See deliverables.

## Comments

This assignment is relatively big:
Please note that this assignment is an OLA Obligatory Learning Activity. You thus have to hand in what ever you have in order to be admitted to the exam. Please see the Semester Description for more details.

## Sources

* 18A Linux as a Switch PDA V0x.docx  
https://gitlab.com/EAL-ITT/19a-itt1-network/blob/master/Literature/18A_Linux_as_a_Switch_PDA_V03.pdf

* Only Section 6.1 in this document:  
https://gitlab.com/EAL-ITT/19a-itt1-network/blob/master/Literature/Python_Network_programming_PDA_V08_p8-18.pdf

* ARP and routing. Only the first two minute.  
https://www.youtube.com/watch?v=cn8Zxh9bPio

* Bridge setup:
http://www.microhowto.info/howto/persistently_bridge_traffic_between_two_or_more_ethernet_interfaces_on_debian.html

* IP program cheat sheet
https://gitlab.com/EAL-ITT/19a-itt1-network/blob/master/Literature/19A_IP_command_cheatsheet_PDA.pdf

* ISO OSI model  
https://gitlab.com/EAL-ITT/19a-itt1-network/blob/master/Literature/1_Datacommunication_OSI_model_V06.pdf


## White Board

TBD

