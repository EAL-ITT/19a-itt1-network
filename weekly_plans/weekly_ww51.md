---
Week: 50
Content:  COURSE WEEK CONTENT
Material: See links in weekly plan
Initials: PDA
---

# Week 51 Networking

## Goals of the week(s)
Pratical and learning goals for the period are as follows

### Practical goals

* See Assignment in Deliverables

### Learning goals

The students can at a basic level work with and explain:

* How to make a default route to give internet access on Junos.
* Security zones and policies on Junos.
* Network Address Translation NAT on Junos. Source natting.
* Troubleshooting  

### Topics to be covered

* Routing on a Juniper physical SRX240
* Security zones and policies on Junos.
* Network Address Translation NAT on Junos.
* How to make a default route on Junos to give internet access to subnets.
* Troubleshooting  

## Deliverables

TBD

* Hand in on PeerGrade. Group hand in.

## Schedule

Class B Tuesday/Class A Wednesday

## Hands-on time

* See deliverables.

## Comments

TBD

## Software

Use Putty to access the routers.

## Sources

* 18A VMware Workstation SRX BRIDGE NAT Internet PDA V06.pdf  
Find it on ITSL in Resources

* Junos Source NAT <br>
Browse to the Example: Configuring Source NAT for Single Address Translation 
https://www.juniper.net/documentation/en_US/junos/topics/topic-map/nat-security-source-and-source-pool.html

* A video on the NAT concept  
https://www.youtube.com/watch?v=FTUV0t6JaDA  

* Troubleshooting  
https://gitlab.com/PerPer/networking/blob/master/Trouble%20shooting/Network_Troubleshooting_Per_Dahlstroem_UCL.md

## White Board

TBD

