---
Week: 45
Content:  COURSE WEEK CONTENT
Material: See links in weekly plan
Initials: PDA
---

# Week 44 - 45 Networking

## Goals of the week(s)
Pratical and learning goals for the period are as follows

### Practical goals

* See Assignment in Deliverables

### Learning goals

The students can at a basic level work with and explain:

* Routing

### Topics to be covered

* Routing on a Juniper SRX240

Linux commands:  

* $ arp See more commands in assignment in deliverables.  

## Deliverables

Assignment 10 Routing, one router, two subnets

Please hand in on PeerGrade.

## Schedule

Class B Tuesday/Class A Wednesday

## Hands-on time

* See deliverables.

## Comments

This assignment is relatively big.

## Software

These three files constitute the vSRX Firefly version 12 for installation on VMWW. Get them from ITSL Resources.

* junos-vsrx-12.1X47-D15.4-domestic.mf
* junos-vsrx-12.1X47-D15.4-domestic.ovf
* junos-vsrx-12.1X47-D15.4-domestic-disk1.vmdk

## Sources

* 18A VMware Juniper Junos vSRX basic routing PDA V02.pdf  
Find it on ITSL in Resources

* Only Section 6.1 in this document:  
https://gitlab.com/EAL-ITT/19a-itt1-network/blob/master/Literature/Python_Network_programming_PDA_V08_p8-18.pdf

* ARP and routing.
https://www.youtube.com/watch?v=cn8Zxh9bPio


* IP program cheat sheet
https://gitlab.com/EAL-ITT/19a-itt1-network/blob/master/Literature/19A_IP_command_cheatsheet_PDA.pdf

* ISO OSI model  
https://gitlab.com/EAL-ITT/19a-itt1-network/blob/master/Literature/1_Datacommunication_OSI_model_V06.pdf


## White Board

TBD

