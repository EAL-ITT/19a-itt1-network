---
Week: 39
Content:  COURSE WEEK CONTENT
Material: See links in weekly plan
Initials: PDA
---

# Week 39 Networking

## Goals of the week(s)

Pratical and learning goals for the period are as follows

### Practical goals

* See Assignment in Deliverables

### Learning goals

The students can at a basic level work with and explain:

* Configure IP addresses on a small Network including subnet masks.
* Identify when IP addresses are on the same IP network and when they are not.
* 
* The student can explain the binary number system and the connection between  
  binary numbers and IP addresses and subnet masks and sub netting.


### Topics to be covered

* Walkthrough and Q&A for handed in homework.
* Subnet masks.
* Network ID.
* Host ID.
* Broadcast address.
* Gateway address.
* Usable IPs on a subnet.
* Private and Public IPs.
* Network Diagrams. Network ID and and Host IPs.
* Two hosts in VMWW.
* Set static IPs.


## Deliverables

Assignment 7 Subnetting.
Assignment 25 Subnetting.

Please hand in on Peergrade in ONE pdf document.

## Schedule

Class B Tuesday/Class A Wednesday

## Hands-on time

* See deliverables.

## Comments

## Sources

TBD

## White Board

This weeks white board scribbles will often be posted here after the lectures  



