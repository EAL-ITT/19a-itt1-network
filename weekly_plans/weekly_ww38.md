---
Week: 38
Content:  COURSE WEEK CONTENT
Material: See links in weekly plan
Initials: PDA
---

# Week 38 network

## Goals of the week(s)
Pratical and learning goals for the period is as follows

### Practical goals

See Deliverables

### Learning goals

Decimal CIDR IP address notation.  
Binary number system.  
Hexadecimal system brief.  
Subnet mask in binary.  

## Deliverables

Assignment 6 Binary number system and IP addresses


## Hands-on time

See Deliverables


## Comments

