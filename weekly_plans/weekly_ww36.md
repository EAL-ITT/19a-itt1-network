---
Week: 36
Content:  COURSE WEEK CONTENT
Material: See links in weekly plan
Initials: PDA
---

# Week 36 network

## Goals of the week(s)
Pratical and learning goals for the period are as follows

### Practical goals

* Install VMWare Workstation.
* Install a Linux Virtual Machine (VM) in VMWare Workstation.
* Connect the VM to the internet via VMnet8 NAT.

### Learning goals

* Install a hardware and network emulation management tool.  
* Install a Linux host on the hardware and network emulation management tool.  
* Superficially explain what a hardware emulator or Hypervisor is.

### Administrative learning goals

* Lecture plan and Weekly plan.
* Assignments.
* Handing in and hand in formats and platforms.

### Topics to be covered

* Introduction to VMware installation.
* VM (Xububtu) installation
* VMnet8 and NAT
* Network Editor.
* How does the internet work? The big picture.
* Buss words: IPV4, MAC, ARP, DHCP.
* Two hosts network.

## Deliverables

Assignment 3 VMWare Workstation installation.

Please hand in on LMS.

## Schedule

Class A Tuesday/Class B Wednesday

## Hands-on time

* Exercise A: Assignment 3 VMWare Workstation installation.

## Comments

## Sources

In general the “PowerCert Animated Videos” seems to be a very good source for Networking topics:
[https://www.youtube.com/channel/UCJQJ4GjTiq5lmn8czf8oo0Q/videos](https://www.youtube.com/channel/UCJQJ4GjTiq5lmn8czf8oo0Q/videos)

## White Board

This weeks white board scribbles will often be posted here after the lectures

![Week 36 White Board](../weekly_plans/Week36_VMware_ecosystem_PDA_V01.jpg "Week 36 White Board scribbles")




