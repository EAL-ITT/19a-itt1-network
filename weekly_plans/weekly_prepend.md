---
title: 'Networking'
subtitle: 'Weekly plans'
authors: ['Per Dahlstrøm \<pda@ucl.dk\>']
main_author: 'Per Dahlstrøm'
date: \today
email: 'pda@ucl.dk'
left-header: \today
right-header: 'network, weekly plans'
---


Introduction
====================

This document is a collection of weekly plans. It is based on the wekly plans in the administrative Gitlab repository.

The sections describe the goals and programm for each week of the course.