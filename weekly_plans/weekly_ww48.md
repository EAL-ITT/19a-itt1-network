---
Week: 48
Content:  COURSE WEEK CONTENT
Material: See links in weekly plan
Initials: PDA
---

# Week 48 Networking

## Goals of the week(s)
Pratical and learning goals for the period are as follows

### Practical goals

* See Assignment in Deliverables

### Learning goals

The students can at a basic level work with and explain:

* Routing between routers on physical routers setting static routes.

### Topics to be covered

* Routing on a Juniper physical SRX240

Linux commands:  

* $ sudo apt install inetutils-traceroute
* $ traceroute IP-address
* $ ping -n IP-address (-n: show IP adresses.)
* $ ping -n 255.255.255.255 (Ping broadcast domain devices.)
* $ tcpdump -i Interface -en icmp (-en: Show both layer 2 and layer 3 info.)
* $ arp -v -n (See more commands in assignment in deliverables.)
* $ man tcpdump; man traceroute; man ping and so on.

## Deliverables

Assignment 28 Routing, two routers, five subnets on physical routers.

* Demonstrate your Network Diagram.
* Demonstrate to the lecturer when the routing is working.

## Schedule

Class B Tuesday/Class A Wednesday

## Hands-on time

* See deliverables.

## Comments

TBD

## Software

Use Putty to access the routers.

## Sources

* 18A VMware Juniper Junos vSRX basic routing PDA V02.pdf  
Find it on ITSL in Resources

* Video on basic routing between two routers also explaining layer 2 activity:
https://www.youtube.com/watch?v=2ydK33mPhTY&t=524s

* Only Section 6.1 in this document:  
https://gitlab.com/EAL-ITT/19a-itt1-network/blob/master/Literature/Python_Network_programming_PDA_V08_p8-18.pdf

* ARP and routing.
https://www.youtube.com/watch?v=cn8Zxh9bPio

* IP program cheat sheet
https://gitlab.com/EAL-ITT/19a-itt1-network/blob/master/Literature/19A_IP_command_cheatsheet_PDA.pdf

* ISO OSI model  
https://gitlab.com/EAL-ITT/19a-itt1-network/blob/master/Literature/1_Datacommunication_OSI_model_V06.pdf


## White Board

TBD

