---
title: '19A ITT1 networking'
subtitle: 'Lecture plan'
authors: ['Per Dahlstrøm \<pda@ucl.dk\>']
main_author: 'Per Dahlstrøm'
date: \today
email: 'pda@ucl.dk'
left-header: \today
right-header: Lecture plan
skip-toc: false
---


# Lecture plan

The lecture plan consists of the following parts: a week/lecture plan, an overview of the distribution of study activities and general information about the course, module or project.

* Study program, class and semester: IT technology, oeait19, 19A+B
* Name of lecturer and date of filling in form: PDA 2019-06-01
* Title of the course, module or project and ECTS: Network, 6 ECTS
* Required readings, literature or technical instructions and other background material: Material will be specified in the weekly plans.

See weekly plan for details like detailed daily plan, links, references, exercises and so on.


-------- ------ ---------------------------------------------
 INIT     Week  Content
-------- ------ ---------------------------------------------
PDA       36    Introduction to Network.

                See in general the Weekly plan for details.
  
PDA       37    Network

                Linux

                Network diagrams
                
PDA       38    Network

                One switch

PDA       39    Network

                Two switches

PDA       40    Network

                One router. Static routes.
                
PDA       41    Network

                Two routers. Static routes.

PDA       42    Network

                Two routers. DHCP.

PDA       43    Network

                Two routers. Connection to the Internet.
                
PDA       44    Network

                OSPF

PDA       45    Network

                OSPF

PDA       46    Network

                TCP UDP Python
                
PDA       47    Network

                TCP UDP Python

PDA       48    Network

                TCP UDP Python

PDA       49    Network

                SSL and TLS

PDA       50    Network

                SSL and TLS

PDA       51    Network

                Project

PDA       1     Network

                Project

PDA       2     Network

                Project

PDA       3     Network
                
                Project
                
---------------------------------------------------------


# General info about the course, module or project

## The student’s learning outcome

Please see the Curriculum2018 section 2. 2.1.

## Content

Please see the weekly plans for detailed content of the course. Also consult the overalle topic by weeks above here.

## Method

The Networking part of the education is a mix of lectures, assignments and practical work on both virtualized and pysical physical network ecosystems in the lab.
During tghe course there is a number of hand ins and peer reviews. I.e. students get mutual inspiration from each other and facilitates their mutual learning processes.

## Equipment

The student is expectd to have a Laptop strong enough to run the newest version of VMware Workstation.

## Projects with external collaborators  (proportion of students who participated)

The subject Project integrates external collaborators and Networking.

## Test form/assessment
The course includes a number of obligatory elements.

See semester plan/description for details on obligatory elements.

## Other general information
None at this time.